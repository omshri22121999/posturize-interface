const express = require('express')
const router = express.Router()
const shell = require('shelljs');

const fs = require('fs')
router.post('/bmist', (req, res) => {
  shell.exec('/home/pruthvirg/posturize-interface/bmi_final_bash.sh',{async:true})
  res.send('hi'); 
})
router.post('/bpst', (req, res) => {
  shell.exec('/home/pruthvirg/posturize-interface/bp_final_bash.sh',{async:true})
  res.send('hi'); 
})
router.post('/ecgst', (req, res) => {
  shell.exec('/home/pruthvirg/posturize-interface/ecg_bash_final.sh',{async:true})
  res.send('hi'); 
})
router.post('/bp', (req, res) => {
  fs.readFile(
    '/home/pruthvirg/Desktop/Posturize/bp_final_status.txt',
    'utf-8',
    function (err, contents) {
  	res.json({'status':contents})
    }
  )
})
router.post('/bmi', (req, res) => {
  fs.readFile(
    '/home/pruthvirg/posturize-interface/bmi_status.txt',
    'utf-8',
    function (err, contents) {
  	res.json({'status':contents})
    }
  )
})
router.post('/ecg', (req, res) => {
  fs.readFile(
    '/home/pruthvirg/Desktop/Posturize/json_for_ecg.txt',
    'utf-8',
    function (err, contents) {
  	res.json({'status':contents})
    }
  )
})
router.get('/bs', (req, res) => {
  console.log("Hi")
})
module.exports = router
