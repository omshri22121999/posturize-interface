var express= require("express")
var bodyParser = require("body-parser")
var cors = require('cors');
const app = express();
const routes = require('./routes/router');

const API_PORT = process.env.API_PORT || 3005;
app.use(
    bodyParser.urlencoded({
        extended: false
    })
);
app.use(bodyParser.urlencoded({
    extended: false
}));

app.use(cors());
app.options('*', cors());

app.use(bodyParser.json());
app.use('/', routes);

app.listen(API_PORT, () => console.log(`Listening on port ${API_PORT}`));
