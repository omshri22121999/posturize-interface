import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { Row, Col, Button } from "react-bootstrap";
class Kannada extends Component {
  constructor(props) {
    super(props);
    this.routeChange = this.routeChange.bind(this);
  }

  routeChange(e) {
    this.props.history.push(e);
  }

  render() {
    return (
      <div className={"container"}>
        <br />
        <Row>
          <Col xsOffset={4}>
            <Button
              bsStyle="primary"
              bsSize="large"
              onClick={() => this.routeChange("/kannada/bp")}
              style={{ height: "80px", width: "300px" }}
            >
              ರಕ್ತದೊತ್ತಡ ಪರೀಕ್ಷಿಸಿ
            </Button>
          </Col>
        </Row>
        <br />
        <br />
        <Row>
          <Col xsOffset={4}>
            <Button
              bsStyle="primary"
              bsSize="large"
              onClick={() => this.routeChange("/kannada/ecg")}
              style={{ height: "80px", width: "300px" }}
            >
              ಇ.ಸಿ.ಜಿ/ಇ.ಕೆ.ಜಿ ಪರೀಕ್ಷಿಸಿ
            </Button>
          </Col>
        </Row>
        <br />
        <br />
        <Row>
          <Col xsOffset={4}>
            <Button
              bsStyle="primary"
              bsSize="large"
              onClick={() => this.routeChange("/kannada/bmi")}
              style={{ height: "80px", width: "300px" }}
            >
              ಬಿ.ಎಂ.ಐ ಪರೀಕ್ಷಿಸಿ
            </Button>
          </Col>
        </Row>
        <br />
        <br />
      </div>
    );
  }
}

export default withRouter(Kannada);
