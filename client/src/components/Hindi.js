import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { Row, Col, Button } from "react-bootstrap";
class Hindi extends Component {
  constructor(props) {
    super(props);
    this.routeChange = this.routeChange.bind(this);
  }

  routeChange(e) {
    this.props.history.push(e);
  }

  render() {
    return (
      <div className={"container"}>
        <br />
        <Row>
          <Col xsOffset={4}>
            <Button
              bsStyle="primary"
              bsSize="large"
              onClick={() => this.routeChange("/hindi/bp")}
              style={{ height: "80px", width: "300px" }}
            >
              रक्त चाप परीक्षा
            </Button>
          </Col>
        </Row>
        <br />
        <br />
        <Row>
          <Col xsOffset={4}>
            <Button
              bsStyle="primary"
              bsSize="large"
              onClick={() => this.routeChange("/hindi/ecg")}
              style={{ height: "80px", width: "300px" }}
            >
              E.C.G / E.K.G परीक्षा
            </Button>
          </Col>
        </Row>
        <br />
        <br />
        <Row>
          <Col xsOffset={4}>
            <Button
              bsStyle="primary"
              bsSize="large"
              onClick={() => this.routeChange("/hindi/bmi")}
              style={{ height: "80px", width: "300px" }}
            >
              B.M.I परीक्षा
            </Button>
          </Col>
        </Row>
        <br />
        <br />
      </div>
    );
  }
}

export default withRouter(Hindi);
