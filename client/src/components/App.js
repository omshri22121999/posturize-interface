import React, { Component } from "react";
import Home from "../container/Home";
import NavTop from "../container/NavTop";
import English from "./English";
import Tamil from "./Tamil";
import Telugu from "./Telugu";
import Kannada from "./Kannada";
import Hindi from "./Hindi";
import EngBPCheck from "./EnglishComp/BPCheck";
import EngBMICheck from "./EnglishComp/BMICheck";
import EngECEKCheck from "./EnglishComp/ECEKCheck";
import TamBPCheck from "./TamilComp/BPCheck";
import TamBMICheck from "./TamilComp/BMICheck";
import TamECEKCheck from "./TamilComp/ECEKCheck";
import KanBPCheck from "./KannadaComp/BPCheck";
import KanBMICheck from "./KannadaComp/BMICheck";
import KanECEKCheck from "./KannadaComp/ECEKCheck";
import TelBPCheck from "./TeluguComp/BPCheck";
import TelBMICheck from "./TeluguComp/BMICheck";
import TelECEKCheck from "./TeluguComp/ECEKCheck";
import HinBPCheck from "./HindiComp/BPCheck";
import HinBMICheck from "./HindiComp/BMICheck";
import HinECEKCheck from "./HindiComp/ECEKCheck";

import { Route } from "react-router-dom";
import { Row, Col } from "react-bootstrap";
import "../css/style.css";
class App extends Component {
  render() {
    return (
      <div className={"container"}>
        <Row>
          <Col>
            <NavTop />
          </Col>
        </Row>
        <Row>
          <Col xsOffset={1}>
            <Route exact path={"/"} component={Home} />
            <Route exact path={"/english"} component={English} />
            <Route exact path={"/tamil"} component={Tamil} />
            <Route exact path={"/telugu"} component={Telugu} />
            <Route exact path={"/kannada"} component={Kannada} />
            <Route exact path={"/hindi"} component={Hindi} />
            <Route exact path={"/english/bp"} component={EngBPCheck} />
            <Route exact path={"/english/bmi"} component={EngBMICheck} />
            <Route exact path={"/english/ecg"} component={EngECEKCheck} />
            <Route exact path={"/tamil/bp"} component={TamBPCheck} />
            <Route exact path={"/tamil/bmi"} component={TamBMICheck} />
            <Route exact path={"/tamil/ecg"} component={TamECEKCheck} />
            <Route exact path={"/telugu/bp"} component={TelBPCheck} />
            <Route exact path={"/telugu/bmi"} component={TelBMICheck} />
            <Route exact path={"/telugu/ecg"} component={TelECEKCheck} />
            <Route exact path={"/kannada/bp"} component={KanBPCheck} />
            <Route exact path={"/kannada/bmi"} component={KanBMICheck} />
            <Route exact path={"/kannada/ecg"} component={KanECEKCheck} />
            <Route exact path={"/hindi/bp"} component={HinBPCheck} />
            <Route exact path={"/hindi/bmi"} component={HinBMICheck} />
            <Route exact path={"/hindi/ecg"} component={HinECEKCheck} />
          </Col>
        </Row>
      </div>
    );
  }
}

export default App;
