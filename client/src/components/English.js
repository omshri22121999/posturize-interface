import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { Row, Col, Button } from "react-bootstrap";
class English extends Component {
  constructor(props) {
    super(props);
    this.routeChange = this.routeChange.bind(this);
  }

  routeChange(e) {
    this.props.history.push(e);
  }

  render() {
    return (
      <div className={"container"}>
        <br />
        <Row>
          <Col xsOffset={4}>
            <Button
              bsStyle="primary"
              bsSize="large"
              onClick={() => this.routeChange("/english/bp")}
              style={{ height: "80px", width: "300px" }}
            >
              Blood Pressure Check
            </Button>
          </Col>
        </Row>
        <br />
        <br />
        <Row>
          <Col xsOffset={4}>
            <Button
              bsStyle="primary"
              bsSize="large"
              onClick={() => this.routeChange("/english/ecg")}
              style={{ height: "80px", width: "300px" }}
            >
              ECG/EKG Check
            </Button>
          </Col>
        </Row>
        <br />
        <br />
        <Row>
          <Col xsOffset={4}>
            <Button
              bsStyle="primary"
              bsSize="large"
              onClick={() => this.routeChange("/english/bmi")}
              style={{ height: "80px", width: "300px" }}
            >
              BMI Check
            </Button>
          </Col>
        </Row>
        <br />
        <br />
      </div>
    );
  }
}

export default withRouter(English);
