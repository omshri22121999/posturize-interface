import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { Row, Col, Button } from "react-bootstrap";
class Tamil extends Component {
  constructor(props) {
    super(props);
    this.routeChange = this.routeChange.bind(this);
  }

  routeChange(e) {
    this.props.history.push(e);
  }

  render() {
    return (
      <div className={"container"}>
        <br />
        <Row>
          <Col xsOffset={4}>
            <Button
              bsStyle="primary"
              bsSize="large"
              onClick={() => this.routeChange("/tamil/bp")}
              style={{ height: "80px", width: "300px" }}
            >
              இரத்த அழுத்த சோதனை
            </Button>
          </Col>
        </Row>
        <br />
        <br />
        <Row>
          <Col xsOffset={4}>
            <Button
              bsStyle="primary"
              bsSize="large"
              onClick={() => this.routeChange("/tamil/ecg")}
              style={{ height: "80px", width: "300px" }}
            >
              ஈ.சி.ஜி / ஈ.கே.ஜி சோதனை
            </Button>
          </Col>
        </Row>
        <br />
        <br />
        <Row>
          <Col xsOffset={4}>
            <Button
              bsStyle="primary"
              bsSize="large"
              onClick={() => this.routeChange("/tamil/bmi")}
              style={{ height: "80px", width: "300px" }}
            >
              பி.எம்.ஐ சோதனை
            </Button>
          </Col>
        </Row>
        <br />
        <br />
      </div>
    );
  }
}

export default withRouter(Tamil);
