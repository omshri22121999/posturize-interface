import React, { Component } from "react";
import axios from "axios";
import { Row, Col, Button } from "react-bootstrap";
import gif from "../../assets/giphy.gif";
import logo from "../../assets/download.jpeg";
class BMICheck extends Component {
  constructor(props) {
    super(props);
    this.state = { status: "Please wait", img: "" };
    this.routeChange = this.routeChange.bind(this);
    this.requestData = this.requestData.bind(this);
  }
  routeChange() {
    this.props.history.push("/english");
  }
  requestData() {
    axios.post(`http://localhost:3001/bp`).then(res => {
      const st = res.data.output;
      this.setState({ status: st });
      if (res.data.img === 1) {
        this.setState({ img: gif });
      } else {
        this.setState({ img: logo });
      }
    });
  }
  componentDidMount() {
    try {
      setInterval(async () => {
        this.requestData();
      }, 1000);
    } catch (e) {
      this.setState({ status: "Please Wait" });
    } finally {
      this.setState({ status: "Please Wait" });
    }
  }

  render() {
    return (
      <div className={"container"}>
        <Row>
          <h1>{this.state.status}</h1>
        </Row>
        <br />
        <br />
        <Row>
          <img src={this.state.img} />
        </Row>
        <br />
        <br />
        <Row className="justify-content-md-center">
          <Col xs lg="4" />
          <Col md="auto">
            <Button
              bsStyle="primary"
              bsSize="large"
              onClick={this.routeChange}
              style={{ height: "50px", width: "250px" }}
            >
              Back
            </Button>
          </Col>
          <Col xs lg="2" />
        </Row>
      </div>
    );
  }
}

export default BMICheck;
