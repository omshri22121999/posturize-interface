import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { Row, Col, Button } from "react-bootstrap";
class Telugu extends Component {
  constructor(props) {
    super(props);
    this.routeChange = this.routeChange.bind(this);
  }

  routeChange(e) {
    this.props.history.push(e);
  }

  render() {
    return (
      <div className={"container"}>
        <br />
        <Row>
          <Col xsOffset={4}>
            <Button
              bsStyle="primary"
              bsSize="large"
              onClick={() => this.routeChange("/telugu/bp")}
              style={{ height: "80px", width: "300px" }}
            >
              రక్తపోటు పరిక్ష
            </Button>
          </Col>
        </Row>
        <br />
        <br />
        <Row>
          <Col xsOffset={4}>
            <Button
              bsStyle="primary"
              bsSize="large"
              onClick={() => this.routeChange("/telugu/ecg")}
              style={{ height: "80px", width: "300px" }}
            >
              E.C.G / E.K.G పరీక్షా
            </Button>
          </Col>
        </Row>
        <br />
        <br />
        <Row>
          <Col xsOffset={4}>
            <Button
              bsStyle="primary"
              bsSize="large"
              onClick={() => this.routeChange("/telugu/bmi")}
              style={{ height: "80px", width: "300px" }}
            >
              B.M.I పరిక్ష
            </Button>
          </Col>
        </Row>
        <br />
        <br />
      </div>
    );
  }
}

export default withRouter(Telugu);
