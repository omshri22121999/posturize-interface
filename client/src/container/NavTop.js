import React from "react";
import { Navbar, Nav, NavItem } from "react-bootstrap";
const NavTop = props => {
  return (
    <Navbar fluid>
      <Navbar.Header>
        <Navbar.Brand>Posturize Interface</Navbar.Brand>
      </Navbar.Header>
      <Nav>
        <NavItem href="/english">English</NavItem>
        <NavItem href="/tamil">தமிழ்</NavItem>
        <NavItem href="/telugu">తెలుగు</NavItem>
        <NavItem href="/kannada">ಕನ್ನಡ</NavItem>
        <NavItem href="/hindi">हिंदी</NavItem>
      </Nav>
    </Navbar>
  );
};
export default NavTop;
