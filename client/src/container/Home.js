import React from "react";
import { PageHeader } from "react-bootstrap";
const Home = props => {
  return (
    <div>
      <PageHeader>HomePage</PageHeader>
      <div class={"container"} style={{ backgroundColor: "powderblue" }}>
        <break />
        <h4>
          Welcome, you can use this machine to check you health. To get started,
          select the language
        </h4>
        <br />
        <br />
        <h4>
          நல்வரவு, நீங்கள் உடல்நலம் சரிபார்க்க இந்த இயந்திரத்தை பயன்படுத்த
          முடியும். தொடங்குவதற்கு, மேலே இருந்து மொழியைத் தேர்ந்தெடுக்கவும்.
        </h4>

        <br />
        <br />
        <h4>
          స్వాగతించండి, మీరు ఈ మెషీన్ను మీకు ఆరోగ్యాన్ని తనిఖీ చేయడానికి
          ఉపయోగించవచ్చు. ప్రారంభించడానికి, ఎగువ నుండి భాషను ఎంచుకోండి.
        </h4>
        <br />
        <br />
        <h4>
          ಸ್ವಾಗತ, ನೀವು ಆರೋಗ್ಯವನ್ನು ಪರೀಕ್ಷಿಸಲು ಈ ಯಂತ್ರವನ್ನು ಬಳಸಬಹುದು.
          ಪ್ರಾರಂಭಿಸಲು, ಮೇಲಿನಿಂದ ಭಾಷೆಯನ್ನು ಆಯ್ಕೆಮಾಡಿ.
        </h4>
        <br />
        <br />
        <h4>
          आपका स्वागत है, आप इस मशीन का उपयोग स्वास्थ्य की जांच करने के लिए कर
          सकते हैं। आरंभ करने के लिए, भाषा का चयन करें।
        </h4>
      </div>
    </div>
  );
};
export default Home;
